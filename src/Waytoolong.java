import java.io.BufferedReader;
import java.io.InputStreamReader;

public class Waytoolong {
	public static String code(String a){
		int l=a.length();
		int lc=l-2;
		char i=a.charAt(0), f=a.charAt(l-1);
		String n= String.valueOf(lc);
		return i+n+f;
	}
	public static void main(String[] args) throws Exception{
		BufferedReader br=new BufferedReader(new InputStreamReader(System .in));
		String s=br.readLine(), out="";
		int n=Integer.parseInt(s), c=0;
		while(n>c){
			c++;
			String p=br.readLine();
			if(p.length()>10){
				out+=code(p)+"\n";
			}
			else{
				out+=p+"\n";
			}
		}
		System.out.println(out);
	}
}
