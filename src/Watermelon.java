import java.io.BufferedReader;
import java.io.InputStreamReader;
public class Watermelon {
	public static String yesorno(int n){
		if(n%2==0 && n!=2){
			return "Yes";
		}
		else return "No";
	}
	public static void main(String[] args) throws Exception{
		BufferedReader br=new BufferedReader(new InputStreamReader(System .in));
		String s=br.readLine();
		int n=Integer.parseInt(s);
		System.out.println(yesorno(n));
	}
}
