//import java.io.BufferedReader;
import java.util.Scanner;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
class Main{
	public static void main(String[] args) throws IOException{
		Scanner br= new Scanner(new InputStreamReader(System.in));
		while(true){
			StringTokenizer st=new StringTokenizer(br.nextLine());
			int N= Integer.parseInt(st.nextToken());
			int B= Integer.parseInt(st.nextToken());
			if (N==-1 && B==-1){
				break;
				}
			int[] arr=new int[N];
			int max=0, min=1,mid;
			for (int i=0;i<N;i++){
				arr[i]=Integer.parseInt(br.nextLine());
				max=Math.max(max, arr[i]);
			}
			int ret=max;
			while(min<=max){
				mid=(min+max)/2;
				int cajas=0;
				for(int i=0;i<N;i++){
					int una=(arr[i]%mid != 0)? 1:0;
					cajas+=arr[i]/mid+una ;
				}
				if(cajas<=B){
					max=mid-1;
					ret=mid;
				}
				else{
					min=mid+1;
				}
			}
			System.out.println(ret);
			br.nextLine();
		}
		br.close();
	}
}