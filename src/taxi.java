import java.util.Scanner;
import java.util.Arrays;
public class taxi {
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		int n=Integer.parseInt(sc.nextLine()),taxi=0;
		int[] grupos=new int[n];
		String[] gru=sc.nextLine().split(" ");
		for (int i=0;i<n;i++){
			grupos[i]=Integer.parseInt(gru[i]);
		}
		Arrays.sort(grupos);
		int inicio=0, fin=n-1;
		while (fin>=inicio){
			taxi++;	
			int resto=4-grupos[fin];
			while (inicio<=fin && resto>=grupos[inicio] ){
				resto-=grupos[inicio];
				inicio++;	
			}
			fin--;
		}
		System.out.print(taxi);
		sc.close();
	}
}
