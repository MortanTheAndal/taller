import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
import java.util.Arrays;
public class VanyaAndTheLanterns{
	public static void main(String[] args) throws IOException{ 
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer(br.readLine());
		int n=Integer.parseInt(st.nextToken());
		int l=Integer.parseInt(st.nextToken());
		int[] arr=new int[n];
		st=new StringTokenizer(br.readLine());
		for(int i=0;i<n;i++){
			arr[i]=Integer.parseInt(st.nextToken());
		}
		Arrays.sort(arr);
		double max=(double)Math.max(arr[0], l-arr[n-1]);
		double min=0;
		for(int i=1;i<n;i++){
			double dif=((double)(arr[i]-arr[i-1]))/2;
			if (dif>min)
				min=dif;
		}
		double d = Math.max(max, min);
		System.out.println(d);
	}
}
