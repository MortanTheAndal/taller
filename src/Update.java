import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
class Update {
	public static void main(String[] args) throws IOException{
		BufferedReader br=new BufferedReader(new InputStreamReader(System .in));
		int tests=Integer.parseInt(br.readLine()), c=0;
		while(c<tests){
			StringBuilder resultados=new StringBuilder();
			StringTokenizer st=new StringTokenizer(br.readLine());
			int size=Integer.parseInt(st.nextToken());
			int[] array=new int[size+1];
			for (int i=0; i<size+1;i++){
				array[i]=0;
			}
			int nupdates=Integer.parseInt(st.nextToken()), cu=0;	
			while(cu<nupdates){
				StringTokenizer updt =new StringTokenizer(br.readLine());
				int I=Integer.parseInt(updt.nextToken()),D=Integer.parseInt(updt.nextToken()),
						plus=Integer.parseInt(updt.nextToken());
				array[I]+=plus;
				array[D+1]-=plus;
				cu++;
			}
			for(int i=1;i<size+1;i++){
				array[i]+=array[i-1];
			}
			int recuests=Integer.parseInt(br.readLine()), cr=0;
			while (cr<recuests){
				resultados.append(array[Integer.parseInt(br.readLine())]);
				resultados.append("\n");
				cr++;
			}
			System.out.print(resultados);
			c++;
		}
	}
}