import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;
class Bbin {
	public static int bin(int[] array, int Q){
		int A=0, B=array.length-1;
		while (A<B){
			int mid = A+(B-A)/2;
			if (Q<=array[mid])
				B=mid;
			else 
				A=mid+1;
		}
		if (array[A]!= Q)
			return -1;
		else 
			return A;
	}
	public static void main(String[] args) throws IOException{
		BufferedReader br= new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer st=new StringTokenizer(br.readLine());
		StringBuilder resultados=new StringBuilder();
		int N=Integer.parseInt(st.nextToken()), Q=Integer.parseInt(st.nextToken());
		int[] array=new int[N];
		st=new StringTokenizer(br.readLine());
		for (int i=0; i<N;i++){
			array[i]=Integer.parseInt(st.nextToken());
		}
		for (int j=0;j<Q;j++){
			int q=Integer.parseInt(br.readLine());
			resultados.append(bin(array,q)); resultados.append("\n");
		}
		System.out.print(resultados);	
	}
}
